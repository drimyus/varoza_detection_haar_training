// VAROZA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <iostream>
#include <stdio.h>

#include <fstream>
#include <string.h>
#include <tchar.h>
#include <Windows.h>
#include <stdlib.h>

#include "sqlite3.h"

using namespace std;
using namespace cv;

std::vector<Rect> detectAndDisplay(Mat image, double scalFactor, int minNeighbors);

string va_cascade_name = "..//data//varoza_haar.xml";
cv::CascadeClassifier va_cascade;

string window_name = "Detected Result";

#include "sqlite3.h" 
static int callback(void *NotUsed, int argc, char **argv, char **azColName) 
{ 
	
	int i; for( i = 0; i < argc; i++) 	
	{ 
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL"); 
	} 
	printf("\n"); 
	return 0; 
};



int main()
{
	int nTotalCnt = 0;

	// Load the cascade
	if (!va_cascade.load(va_cascade_name)) {
		printf("ERROR: Loading Cascade .xml file.\n");
		return -1;
	}

	// Search the whole image files in specific directory
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;

	TCHAR * directory = TEXT("..//data//test_images");

	char chaTempDirectory[MAX_PATH];	
	char chaTempFileName[MAX_PATH];
	char DefChar = ' ';

	// For storing the data to Sqlite database 
	sqlite3 *pSQlite3 = NULL;
	char *szErrMsg = NULL;
	
	int rst;

	WideCharToMultiByte(CP_ACP, 0, directory, -1, chaTempDirectory, 260, &DefChar, NULL);

	TCHAR patter[MAX_PATH];

	memset(patter, 0x00, MAX_PATH);
	_stprintf_s(patter, TEXT("%s//*.jpg"), directory);
	hFind = FindFirstFile(patter, &FindFileData);
	

	if (hFind == INVALID_HANDLE_VALUE) {
		printf("ERROR: FindFirstFile failed.\n");
		return -1;
	}


	// Open the database
	rst = sqlite3_open("result.db", &pSQlite3);
	if (rst) {
		fprintf(stderr, "Error: Cannot open the database.%s\n", sqlite3_errmsg(pSQlite3));
		sqlite3_close(pSQlite3);
		pSQlite3 = NULL;
		return -1;
	}
	else {
		fprintf(stderr, "Opened database successfully.\n");
	}
		
	// Create the table on db
	rst = sqlite3_exec(pSQlite3,
		"CREATE TABLE IF NOT EXISTS member ( file_name TEXT, count_varozas INTEGER );",
		callback, 0, &szErrMsg);	
	if (rst != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", szErrMsg);
		sqlite3_free(szErrMsg);
		return -1;
	}
	else {
		fprintf(stdout, "Table created successfully.\n");
	}
	

	do {
		// Ignore current and parent directories
		if (_tcscmp(FindFileData.cFileName, TEXT(".")) == 0 || _tcscmp(FindFileData.cFileName, TEXT("..")) == 0)
			continue;
		
		if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// Ignore directories
		}
		else
		{
			//list the Files
			//_tprintf(TEXT("%s\n"), FindFileData.cFileName);					

			//wide char array
			//FindFileData.cFileName
			//convert "FindFileData.cFileName" from (wide char) to "chaTempFileName"(narrow char array)							
			WideCharToMultiByte(CP_ACP, 0, FindFileData.cFileName, -1, chaTempFileName, 260, &DefChar, NULL);

			//A std:string  using the char* constructor.
			std::string strInFileName(chaTempFileName);
			std::string strInFileDirectory(chaTempDirectory);

			std::string strFullPath = strInFileDirectory + "//" + strInFileName;			
			
			
			Mat image = imread(strFullPath);
			if (!image.empty()) {
				
				// the dst image size,e.g. 250 x 250
				Size size(800, 600);
				cv::resize(image, image, size);

				std::vector<Rect> rects = detectAndDisplay(image, 1.01, 1);

				// Insert Data to table
				char sql[100];
				sprintf(sql, "INSERT INTO member(file_name, count_varozas) values ('%s', %d);", strInFileName.c_str(), (int)rects.size());							
				//cout << sql << endl;				
				rst = sqlite3_exec(pSQlite3,
					sql,
					callback, 0, &szErrMsg
				);

				if (rst != SQLITE_OK) {
					fprintf(stderr, "SQL error: %s\n", szErrMsg);
					sqlite3_free(szErrMsg);
					return -1;
				}
				else {
					fprintf(stdout, "Recode created successfully.\n");
				}


				cout << strInFileName << ":" << rects.size() << endl;
				
				nTotalCnt++;
			}
			else {
				printf("ERROR: Can not read image file :\n");
				continue;
			}

		}

	} while (FindNextFile(hFind, &FindFileData));

	FindClose(hFind);	
	
	sqlite3_free(szErrMsg);
	sqlite3_close(pSQlite3);
	
	return 0;
	
}

std::vector<Rect> detectAndDisplay(Mat image, double scalFactor, int minNeighbors)
{
	std::vector<Rect> varozas;	
	Mat gray;	
	int uId = 0;

	cv::cvtColor(image, gray, cv::COLOR_RGB2BGR);

	// Detect the varoza	
	va_cascade.detectMultiScale(gray, varozas, scalFactor, minNeighbors);
	
	// Show the detected result with rectangle
	for (int i = 0; i < varozas.size(); i++)
	{
		// Index of varozas
		uId++;

		// Draw the rectangle on the detected varoza
		Point pt1(varozas[i].x, varozas[i].y);
		Point pt2((varozas[i].x + varozas[i].height), (varozas[i].y + varozas[i].width));
		rectangle(image, Rect(pt1, pt2), Scalar(0.255, 255), 2, 8, 0);

		stringstream sstm;
		sstm << "Id " << uId;
		putText(image, sstm.str(), pt1, FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 255, 255), 2, CV_AA);
		
	}
	cv::imshow("Result", image);

	if (cv::waitKey(1000) == 0x27) {
		// break;
	}

	return varozas;

}