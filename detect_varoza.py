import cv2

PREDICTOR_PATH = 'myhaar.xml'

my_cascade = cv2.CascadeClassifier(PREDICTOR_PATH)


img = cv2.imread('test_images/DaEHMb.jpg')
img = cv2.resize(img, (800, 600))
gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

rects = my_cascade.detectMultiScale(gray, 1.1, 3)
print len(rects)

for (x, y, w, h) in rects:
    cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
    roi_gray = gray[y:y + h, x:x + w]
    roi_color = img[y:y + h, x:x + w]


cv2.imshow("Result", img)
cv2.waitKey(0)
